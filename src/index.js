//console.log('hola mundo cruel a devil');
var jquery = require("jquery");
window.$ = window.jQuery = jquery; // notice the definition of global variables here
require("jquery-ui-dist/jquery-ui.js");

import Vue from '../node_modules/vue/dist/vue.common.js';
import FormGeneric from './FormGeneric.vue';
import { CollapseTransition } from 'vue2-transitions';

Vue.component(FormGeneric.name, FormGeneric);
Vue.component(CollapseTransition.name, CollapseTransition);
// console.log(FormGeneric);
Document.prototype.ready = callback => {
  const stateIsReady = state => state === 'interactive' || state === 'complete';
  if (callback && typeof callback === 'function') {
    if (stateIsReady(document.readyState)) {
      return callback();
    }
    document.addEventListener('DOMContentLoaded', () => {
      if (stateIsReady(document.readyState)) {
        return callback();
      }
    });
  }
};
document.ready(() => {
  const app = new Vue({
    el: '#app',
    data: {
      showTerms: false,
      showModel: {
        sportage: false,
        sorento: false
      }
    }
  });

  window.$app = app;
});
$(document).ready(function(){
    // Initialize Tooltip
    $('[data-toggle="tooltip"]').tooltip(); 
    
    // Add smooth scrolling to all links in navbar + footer link
    $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
  
      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== "") {
  
        // Prevent default anchor click behavior
        event.preventDefault();
  
        // Store hash
        var hash = this.hash;
  
        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 900, function(){
     
          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        });
      } // End if
    });
  })